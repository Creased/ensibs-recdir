.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\gdi32.inc
include D:\bin\masm32\include\gdiplus.inc
include D:\bin\masm32\include\user32.inc
include D:\bin\masm32\include\shell32.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc
include D:\bin\masm32\include\comctl32.inc
include D:\bin\masm32\include\shlwapi.inc

includelib D:\bin\masm32\lib\gdi32.lib
includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\user32.lib
includelib D:\bin\masm32\lib\shell32.lib
includelib D:\bin\masm32\lib\msvcrt.lib
includelib D:\bin\masm32\lib\comctl32.lib
includelib D:\bin\masm32\lib\shlwapi.lib

.CONST
    windowClassName          db "Listing", 0
    windowTitle              db "Listing", 0

    inputText                db  "D:\", 0
    inputTop                 equ 15
    inputLeft                equ 15
    inputHeight              equ 20
    inputWidth               equ 650
    inputIdName              db  "Edit", 0
    inputId                  equ 1001

    buttonCaption            db  "List", 0
    buttonTop                equ 15
    buttonLeft               equ 680
    buttonHeight             equ 20
    buttonWidth              equ 100
    buttonIdName             db  "Button", 0
    buttonId                 equ 1002

    treeViewCaption          db  "Files", 0
    treeViewTop              equ 45
    treeViewLeft             equ 15
    treeViewHeight           equ 400
    treeViewWidth            equ 765
    treeViewIdName           db  "SysTreeView32", 0
    treeViewId               equ 1003

    windowHeight             equ 500
    windowWidth              equ 800

.DATA

.DATA?
    initPath        db         ?
    windowMessage   MSG        <>
    windowClass     WNDCLASSEX <>
    windowHandler   HWND       ?
    buttonHandler   HWND       ?
    inputHandler    HWND       ?
    treeViewHandler HWND       ?

.CODE
    draw_window PROC
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        sub esp, 8        ; prépare de la place sur la stack pour stocker les variables locales
                          ; window_handler: ebp - 8 (4 octets)
                          ; gui_handler: ebp - 4 (4 octets)

        ; registers and initializes certain common control window classes.
        call InitCommonControls

        ; returns a handle to the file used to create the calling process (.exe file).
        push NULL
        call GetModuleHandle
        mov [ebp-4], eax

        ; sizeof(WNDCLASS) = 40
        ; UINT      style; (4 = 0)
        ; WNDPROC   lpfnWndProc; (4 = 4)
        ; int       cbClsExtra; (4 = 8)
        ; int       cbWndExtra; (4 = 12)
        ; HINSTANCE hInstance; (4 = 16)
        ; HICON     hIcon; (4 = 20)
        ; HCURSOR   hCursor; (4 = 24)
        ; HBRUSH    hbrBackground; (4 = 28)
        ; LPCTSTR   lpszMenuName; (4 = 32)
        ; LPCTSTR   lpszClassName; (4 = 36)

        ; init window class.
        mov [windowClass.cbSize], SIZEOF WNDCLASSEX ; WNDCLASS.cbSize
        mov [windowClass.style], CS_HREDRAW or CS_VREDRAW ; WNDCLASS.style
        mov [windowClass.lpfnWndProc], handle_window ; WNDCLASS.lpfnWndProc
        mov [windowClass.cbClsExtra], NULL ; WNDCLASS.cbClsExtra
        mov [windowClass.cbWndExtra], NULL ; WNDCLASS.cbWndExtra

        mov eax, ebp
        sub eax, 4
        push eax ; hInstance
        mov [windowClass.hInstance], eax ; WNDCLASS.hInstance

      	push IDI_APPLICATION
      	push NULL
        call LoadIcon ; LoadIcon(hInstance, lpIconName);
        mov [windowClass.hIcon], eax ; WNDCLASS.hIcon
        mov [windowClass.hIconSm], eax ; WNDCLASS.hIconSm

        push IDC_ARROW
        push NULL
        call LoadIcon ; LoadCursor(hInstance, lpCursorName);
        mov [windowClass.hCursor], eax ; WNDCLASS.hCursor

        mov [windowClass.hbrBackground], COLOR_WINDOW ; WNDCLASS.lpszMenuName
        mov [windowClass.lpszMenuName], NULL ; WNDCLASS.lpszMenuName
        mov [windowClass.lpszClassName], offset windowClassName ; WNDCLASS.lpszClassName

        ; register window class.
        push offset windowClass
        call RegisterClassEx

        ; creates a window with an extended window style.
        push NULL ; lpParam
        mov eax, ebp
        sub eax, 4
        push eax ; hInstance
        push NULL ; hMenu
        push NULL ; hWndParent
        push offset windowHeight ; nHeight
        push offset windowWidth ; nWidth
        push CW_USEDEFAULT ; y
        push CW_USEDEFAULT ; x
        push WS_SYSMENU ; dwStyle
        push offset windowTitle ; lpWindowName
        push offset windowClassName ; lpClassName
        push WS_EX_CLIENTEDGE ; dwExStyle: the window has a border with a sunken edge.
        call CreateWindowEx ; CreateWindowEx(dwExStyle, lpClassName, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
        mov [windowHandler], eax ; store window handler on the stack.

        push SW_SHOW ; show and display window flag
        push [windowHandler]
        call ShowWindow ; show and display window

        push [windowHandler]
        call UpdateWindow ; update window

        ; Add path input
        push NULL                  ; lpParam
        mov eax, ebp
        sub eax, 4
        push eax ; hInstance
        push inputId               ; hMenu
        push [windowHandler]       ; hWndParent
        push inputHeight           ; nHeight
        push inputWidth            ; nWidth
        push dword ptr inputTop    ; y
        push dword ptr inputLeft   ; x
        push WS_CHILD+WS_VISIBLE+WS_BORDER ; dwStyle
        push offset inputText      ; lpWindowName
        push offset inputIdName    ; lpClassName
        push 0                     ; dwExtStyle
        call CreateWindowEx ; CreateWindowEx(dwExStyle, lpClassName, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
        mov [inputHandler], eax ; store input handler on the stack.

        ; Add button
        push NULL                  ; lpParam
        mov eax, ebp
        sub eax, 4
        push eax ; hInstance
        push buttonId              ; hMenu
        push [windowHandler]       ; hWndParent
        push buttonHeight          ; nHeight
        push buttonWidth           ; nWidth
        push dword ptr buttonTop   ; y
        push dword ptr buttonLeft  ; x
        push WS_CHILD + WS_VISIBLE ; dwStyle
        push offset buttonCaption  ; lpWindowName
        push offset buttonIdName   ; lpClassName
        push 0                     ; dwExtStyle
        call CreateWindowEx ; CreateWindowEx(dwExStyle, lpClassName, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
        mov [buttonHandler], eax ; store button handler on the stack.

        ; Add treeview
        push NULL                  ; lpParam
        mov eax, ebp
        sub eax, 4
        push eax ; hInstance
        push treeViewId             ; hMenu
        push [windowHandler]        ; hWndParent
        push treeViewHeight         ; nHeight
        push treeViewWidth          ; nWidth
        push dword ptr treeViewTop  ; y
        push dword ptr treeViewLeft ; x
        push WS_CHILD + WS_VISIBLE + TVS_HASLINES + TVS_HASBUTTONS + TVS_LINESATROOT  ; dwStyle
        push offset treeViewCaption ; lpWindowName
        push offset treeViewIdName  ; lpClassName
        push 0                      ; dwExtStyle
        call CreateWindowEx ; CreateWindowEx(dwExStyle, lpClassName, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);
        mov [treeViewHandler], eax ; store treeview handler on the stack.

        message_loop:
            push 0
            push 0
            push NULL
            push offset windowMessage
            call GetMessage ; GetMessage(lpMsg, hWnd, wMsgFilterMin, wMsgFilterMax);

            test eax, eax
            jz return_window

            push offset windowMessage
            call TranslateMessage

            push offset windowMessage
            call DispatchMessage

            jmp message_loop

        return_window:
            mov esp, ebp  ; retour sur l'ancienne stackframe
            pop ebp       ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret           ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    draw_window ENDP

    handle_window PROC
      	; https://msdn.microsoft.com/fr-fr/library/windows/desktop/ms633573.aspx
      	; [ebp+8]: hwnd, A handle to the window.
      	; [ebp+12]: uMsg, The message.
      	;	[ebp+16]: wParam, Additional message information. The contents of this parameter depend on the value of the uMsg parameter.
      	;	[ebp+20]: lParam, Additional message information. The contents of this parameter depend on the value of the uMsg parameter.

        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

      	; window destroy handler
      	cmp WORD PTR[ebp+12], WM_DESTROY
      	je window_destroy

        ; define window proc
      	push [ebp+20]
      	push [ebp+16]
      	push [ebp+12]
      	push [ebp+8]
      	call DefWindowProc
      	je return_window

      	window_destroy:
            push NULL
            call PostQuitMessage

      	return_window:
            mov esp, ebp  ; retour sur l'ancienne stackframe
            pop ebp       ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret           ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    handle_window endp

    start:
        call draw_window ; appel de la fonction d'affichage de la fenêtre

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);

    end start
