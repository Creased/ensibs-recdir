@echo off

REM /c Assemble without linking
REM /Zd Add line number debug info
REM /coff generate COFF format object file

D:\bin\masm32\bin\ml /c /Zd /coff recdir.asm
D:\bin\masm32\bin\Link /SUBSYSTEM:CONSOLE recdir.obj

pause
