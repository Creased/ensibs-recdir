# Assembleur x86 - Projet DIR récursif

## Énoncé

> L'application a réaliser dans ce projet est un clone de la fonction Windows `DIR /s` permettant de lister récursivement les fichiers présents sur le disque à partir d'un point d'entrée fourni par l'utilisateur.
>
> L'application pourra, dans un premier temps être développée dans une version ligne de commande uniquement. Cependant, une seconde version devra disposer d'une interface graphique permettant à l'utilisateur d'entrer le point d'entrée dans un champ spécifique (`DialogBoxParam`). De plus, le listing de fichier sera affiché dans une fenêtre permettant de naviguer (scrolling) dans le listing.
>
> Le développeur devra penser à l'ergonomie de l'affichage afin de faciliter la lecture du listing de fichier produit.

## Problématique

L'objectif de ce projet est de nous sensibiliser à la programmation et à l'analyse de
code bas niveau en utilisant le langage assembleur MASM32.

### Approche théorique

En utilisant l'API Windows, nous disposons de quelques fonctions pouvant nous aider
pour lister les fichiers d'un répertoire, notamment [`FindFirstFile`](https://msdn.microsoft.com/en-us/library/windows/desktop/aa364418.aspx) et
[`FindNextFile`](https://msdn.microsoft.com/en-us/library/windows/desktop/aa364428.aspx).

L'algorithme qui sera utilisé sera simple et reviendra à implémenter le principe
de fonctionnement suivant&nbsp;:

![dir](illustrations/dir.png)

Le pseudo-code suivant peut également servir de base pour la conception de notre
programme&nbsp;:

```raw
// SOUS-PROGRAMME: RECDIR
	// ARGUMENTS
		// ENTRÉE
			// directory: chaîne de caractères (chemin absolu vers le répertoire à analyser)
		// SORTIE
      // vide
	// DEBUT
    // Ajoute un wildcard (joker) permettant de lister tous les fichiers
		search_context <- CONCAT(directory, '/*')
    listing_handler <- FindFirstFile(search_context, find_file_data)
    FAIT
        SI find_file_data est un dossier
            next_dir <- CONCAT(directory, '/', find_file_data)
            APPEL RECDIR(next_dir)

        AFFICHE find_file_data
    TANT QUE FindNextFile(listing_handler, find_file_data)
	// FIN SOUS-PROGRAMME
```

Ici on remarque rapidement quelques difficultés que l'on risque de rencontrer
lors de l'implémentation de l'algorithme en langage C ou en MASM32&nbsp;:

 - **La gestion de la mémoire&nbsp;:** de nombreuses variables vont être utilisées
   au cours des différents appels à notre fonction récursive, il faudra donc
   gérer de manière optimale le stockage de ces variables pour garantir un accès
   fiable et une isolation entre les différents appels&nbsp;;
 - **La concaténation&nbsp;:** la gestion de l'espace mémoire pour le stockage des
   données à concaténer sera certainement plus complexe en utilisant un langage
   de haut niveau comme le langage C, mais sera sans doute simplifiée en utilisant
   le langage assembleur MASM32&nbsp;;
 - **L'affichage et le formatage des données pour l'utilisateur&nbsp;:** on peut
   imaginer que les appels récursifs à la fonction vont sans doute complexifier
   l'affichage et le formatage des données pour l'utilisateur. Le choix a été fait
   ici de rentrer dans les répertoires à mesure qu'on en trouve un nouveau, une vue
   représentée sous la forme d'un arbre sera certainement le meilleur choix pour permettre
   une représentation claire et familière à l'utilisateur, par exemple&nbsp;:

```raw
Répertoire racine : D:\test\

|--a/
|----b/
|------c.txt
|------d/
|--------e.txt
|----f.txt
|--g.txt
|--h/
```

### Approche pratique (implémentation en C)

**Avertissement&nbsp;:** Suite à quelques problèmes rencontrés lors de l'implémentation du projet en
utilisant le langage C, il s'est avéré nécessaire l'utilisation d'un système de
formatage particulier pour les chaînes de caractères. Ainsi, l'utilisation de [`TCHAR`](https://msdn.microsoft.com/fr-fr/library/windows/desktop/ff381407.aspx)
a rendu plus complexe l'interprétation du code et son implémentation.

#### Variables et structures utiles

L'utilisation des fonctions `FindFirstFile` et `FindNextFile` nous permet
l'utilisation de deux variables&nbsp;:

 - **Le *handler*&nbsp;:** il s'agit d'un descripteur de fichier décrivant l'état
   de notre recherche et permettant l'interfaçage avec les fonctions `FindNextFile`
   et `FindClose`&nbsp;:
```c
HANDLE listing_handler;
```
 - **La structure de données&nbsp;:** cette structure contient un certain nombre d'informations
   utiles sur le fichier actuellement ciblé, notamment son nom, sa date de création,
   etc.&nbsp;:
```c
WIN32_FIND_DATAW find_file_data;
```

#### Concaténation

Pour lister les fichiers disponibles dans un répertoire, il faut procéder
à l'ajout par concaténation d'un wildcard (`*`) permettant de spécifier le périmètre
de recherche de nos fichiers à la fonction `FindFirstFile` (*scope*).

Afin de prendre en charge l'utilisation de chaîne de caractères saisies par l'utilisateur
et issues de la fonction `scanf`, il est nécessaire de procéder à l'implémentation d'un
code spécifique permettant la suppression des sauts de ligne
(marquage utilisé par `scanf` pour décrire la fin du flux)
pour la concaténation des chaînes&nbsp;:

```c
void wc_copy(wchar_t * target, wchar_t * source) {
    while (* source != '\0' && * source != '\n') {
        *target = *source;
        source++;
        target++;
    }
    *target = '\0';
}
```

Ce code permet simplement de s'appuyer sur des pointeurs de caractères
(pour rappel, une chaîne en C est un tableau contenant des pointeurs de caractères)
pour procéder à une copie profonde (copie de valeur) vers un nouvel emplacement mémoire.
Ici, on va copier tous les caractères jusqu'à trouver un caractère marquant un saut de
ligne ou une fin de chaîne (null byte) afin de ne pas inclure un potentiel saut de ligne
dans une future concaténation.

Finalement, on peut enfin concaténer deux chaînes sans problèmes en utilisant la
fonction `strcpy` (ici on utilise `wcscat`, mais c'est la même fonction)&nbsp;:

```c
// Allocation d'un emplacement mémoire permettant la concaténation du chemin actuel
// et d'un caractère joker (wildcard)
wchar_t * path = (wchar_t *) malloc(wcslen(dir_) + wcslen(WILDCARD) + 1);
wc_copy(path, dir_);
wcscat(path, WILDCARD);
```

#### Résultats

Le résultat est plutôt proche de ce que nous souhaitions obtenir, le dir récursif
fonctionne et permet un affichage semblable à celui décrit précédemment&nbsp;:

![c_code](illustrations/c_code.png)

Le code complet est disponible dans le répertoire [c/](c/).

### Implémentation en MASM32

Suite aux différents TP réalisés en utilisant le langage MASM32, aucune difficulté
particulière n'a été rencontrée pour implémenter le projet en assembleur.

Le calcul rigoureux de l'espace mémoire nécessaire pour l'allocation des différentes
variables et structures sur la stack a permit une implémentation simplifiée et fluide
du code précédemment produit en langage C.

### Conclusion

*A priori*, l'implémentation en utilisant le langage C peut constituer une perte
de temps. En effet il faut noter que j'ai finalement passé plus de temps sur
l'implémentation du projet en utilisant le langage C qu'en utilisant le MASM32,
notamment à cause des problèmes relatifs au typage des données, ce qui n'est pas
le cas en utilisant le MASM32 puisque seule la taille des données compte&nbsp;!
Néanmoins, une première implémentation en utilisant le langage C permet
de cibler immédiatement les potentiels points bloquant et de se familiariser
avec les fonctions et spécificités de l'API Windows, ce qui constitue finalement
un net gain de temps lors du passage en MASM32.

Après un démarrage difficile, notamment dû à une perte de temps sur les précédents
TP, je regrette toutefois ne pas avoir pu profiter de plus de temps pour procéder à une
implémentation correcte et aboutie de la GUI...
