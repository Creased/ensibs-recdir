#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define MAXPATH 2048
#define WILDCARD L"\\*"

void wc_copy(wchar_t * target, wchar_t * source) {
    while (* source != '\0' && * source != '\n') {
        *target = *source;
        source++;
        target++;
    }
    *target = '\0';
}

int recdir(wchar_t * dir_, int index) {
    // Directory listing handler (cf. https://go.bmoine.fr/3pf16CDrzUxfJ6).
    HANDLE listing_handler;
    WIN32_FIND_DATAW find_file_data;
    DWORD error_code = 0, i = 0;

    wchar_t next_path[MAXPATH];

    // Append wildcard to pathname.
    // Issue with wcscpy: EOF is copied...
    wchar_t * path = (wchar_t *) malloc(wcslen(dir_) + wcslen(WILDCARD) + 1);
    wc_copy(path, dir_);
    // wcscpy(path, dir_);
    wcscat(path, WILDCARD);

    // Directory listing based on wildcard pathname.
    //
    // HANDLE WINAPI FindFirstFile(
    //   _In_  LPCTSTR           lpFileName,
    //   _Out_ LPWIN32_FIND_DATA lpFindFileData
    // );
    //
    listing_handler = FindFirstFileW(path, &find_file_data);

    if (listing_handler == INVALID_HANDLE_VALUE) {
        // If FindFirstFile returns -1.
        wprintf(L"An error occured while processing your directory (\"%ws\")...", path);
    } else {
        if (wcslen(path) > MAX_PATH) {
            wprintf(L"Directory path is too long!\n");
        } else {
            // List all the files in the directory with some info about them.
            do {
                if (wcsncmp(find_file_data.cFileName, L".", MAX_PATH) && wcsncmp(find_file_data.cFileName, L"..", MAX_PATH)) {
                    wprintf(L"|");
                    for (i = 0; i <= index; i++) {
                        wprintf(L"--");
                    }
                    if (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {  // Directory
                        wprintf(L"%ws/\n", find_file_data.cFileName);
                        wc_copy(next_path, dir_);
                        wcscat(next_path, L"\\");
                        wcscat(next_path, find_file_data.cFileName);
                        recdir(next_path, index + 1);
                    } else {  // File
                        wprintf(L"%ws\n", find_file_data.cFileName);
                    }
                }
            } while (FindNextFileW(listing_handler, &find_file_data) != 0);
        }

        // Close directory listing handler.
        FindClose(listing_handler);
    }

    free(path);

    error_code = GetLastError();

    return error_code;
}

int main(int argc, char *argv[]) {
    wchar_t init_path[MAXPATH];
    wprintf(L"Your directory ? #> ");
    fgetws(init_path, MAXPATH + 1, stdin);
    wprintf(L"\nTarget directory is %ws\n", init_path);
    recdir(init_path, 0);
    return 0;
}
