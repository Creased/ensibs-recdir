.386
.model flat,stdcall
option casemap:none

include D:\bin\masm32\include\windows.inc
include D:\bin\masm32\include\kernel32.inc
include D:\bin\masm32\include\msvcrt.inc

includelib D:\bin\masm32\lib\kernel32.lib
includelib D:\bin\masm32\lib\msvcrt.lib

.DATA
    userInputPrompt          db "Your directory ? #> ", 0
    targetDirectoryPrompt    db 10, "Target directory is %s", 10, 0
    scanfFormat              db "%s", 0
    wildcard                 db "\*", 0
    backslash                db "\", 0
    findFirstFileErrorPrompt db "An error occured while processing your directory (", 34, "%s", 34,")...", 0
    directorySizeErrorPrompt db "Directory path is too long!", 10, 0
    filePrompt               db "%s", 10, 0
    dirPrompt                db "%s/", 10, 0
    dot                      db ".", 0
    double_dot               db "..", 0
    pipe                     db "|", 0
    line                     db "--", 0

.DATA?
    initPath db ?

.CODE
    recdir PROC
        ; création d'une nouvelle stack frame
        push ebp          ; place le bas de pile (ebp) en haut de pile (esp)
        mov ebp, esp      ; place la valeur de esp dans ebp

        sub esp, 8792     ; prépare de la place sur la stack pour stocker les variables locales
                          ; i: ebp - 8792 (4 octets)
                          ; &listing_handler: ebp - 8788 (4 octets)
                          ; &find_file_data: ebp - 8784 (592 octets)
                          ; next_path: ebp - 8192 (4096 octets)
                          ; path: ebp - 4096 (4096 octets)

        xor ebx, ebx
        mov [ebp-8792], ebx   ; location of variable i

        xor eax, eax       ; mise à zero du registre eax
        mov ebx, [ebp+8]   ; récupère la chaîne de caractères à traiter
                           ; depuis la stack et la place dans le registre esi

        mov eax, ebp     ; sauvegarde le pointeur de bas de pile dans eax
        sub eax, 4096    ; eax -> ebp-4096
        push ebx         ; &dir_
        push eax         ; &path
        call crt_strcpy  ; strcpy(&path, &dir_);

        mov eax, ebp         ; sauvegarde le pointeur de bas de pile dans eax
        sub eax, 4096        ; eax -> ebp-4096
        push offset wildcard ; &wildcard ("\*")
        push eax             ; &path
        call crt_strcat      ; strcat(&path, &wildcard);

        mov eax, ebp         ; sauvegarde le pointeur de bas de pile dans eax
        sub eax, 4096        ; eax -> ebp-4096
        mov ebx, ebp         ; sauvegarde le pointeur de bas de pile dans ebx
        sub ebx, 8784        ; ebx -> ebp-8784
        push ebx             ; &find_file_data
        push eax             ; &path
        call FindFirstFile
        mov [ebp-8788], eax  ; listing_handler = FindFirstFile(&path, &find_file_data);

        ; if (listing_handler == INVALID_HANDLE_VALUE)
        cmp eax, -1
        jne check_dir_size

        mov eax, ebp         ; sauvegarde le pointeur de bas de pile dans eax
        sub eax, 4096        ; eax -> ebp-4096
        push eax             ; path
        push offset findFirstFileErrorPrompt
        call crt_printf      ; printf("An error occured while processing your directory (\"%s\")...", path);

        check_dir_size:
            mov eax, ebp         ; sauvegarde le pointeur de bas de pile dans eax
            sub eax, 4096        ; eax -> ebp-4096
            push eax             ; &path
            call crt_strlen      ; strlen(&path);
            cmp eax, 260         ; if (strlen(&path) > MAX_PATH)
            jbe list_file

            push offset directorySizeErrorPrompt
            call crt_printf ; printf("Directory path is too long!\n");

        list_file:
            ; DWORD    dwFileAttributes;   0
            ; FILETIME ftCreationTime;     4
            ; FILETIME ftLastAccessTime;   12
            ; FILETIME ftLastWriteTime;    20
            ; DWORD    nFileSizeHigh;      28
            ; DWORD    nFileSizeLow;       32
            ; DWORD    dwReserved0;        36
            ; DWORD    dwReserved1;        40
            ; TCHAR    cFileName[MAX_PATH];44
            ; TCHAR    cAlternateFileName[14];

            mov eax, ebp         ; sauvegarde le pointeur de bas de pile dans eax
            sub eax, 8784        ; eax -> ebp-8784
            add eax, 44          ; offset for cFileName
            mov ebx, 260
            push ebx
            push offset dot      ; "."
            push eax             ; push eax on the stack (find_file_data.cFileName)
            call crt_strcmp      ; strcmp(find_file_data.cFileName, ".", MAX_PATH)
            test eax, eax
            je next

            mov eax, ebp          ; sauvegarde le pointeur de bas de pile dans eax
            sub eax, 8784         ; eax -> ebp-8784
            add eax, 44           ; offset for cFileName
            mov ebx, 260
            push ebx
            push offset double_dot ; ".."
            push eax               ; push eax on the stack (find_file_data.cFileName)
            call crt_strcmp        ; strcmp(find_file_data.cFileName, "..", MAX_PATH)
            test eax, eax
            je next

            push offset pipe
            call crt_printf  ; printf("|");

            xor eax, eax
            mov [ebp-8792], eax

            for_i: ; for (i = 0; i <= index; i++)
                push offset line
                call crt_printf  ; printf("--");

                mov ebx, [ebp+12]
                mov eax, [ebp-8792]
                inc eax
                mov [ebp-8792], eax

                cmp eax, ebx
                jle for_i

            ; if (find_file_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            mov eax, [ebp-8784]
            and eax, 16
            jz file_prompt

            mov eax, ebp           ; sauvegarde le pointeur de bas de pile dans eax
            sub eax, 8784          ; eax -> ebp-8784
            add eax, 44            ; offset for cFileName
            push eax               ; push eax on the stack (find_file_data.cFileName)
            push offset dirPrompt  ; push eax on the stack (find_file_data.cFileName)
            call crt_printf        ; printf("%s\n", find_file_data.cFileName);

            mov ebx, [ebp+8]   ; récupère la chaîne de caractères à traiter
                               ; depuis la stack et la place dans le registre ebx
            mov eax, ebp       ; sauvegarde le pointeur de bas de pile dans eax
            sub eax, 8192      ; eax -> ebp-8192
            push ebx           ; chaîne utilisateur
            push eax           ; next_path
            call crt_strcpy    ; strcpy(next_path, dir_);

            mov eax, ebp          ; sauvegarde le pointeur de bas de pile dans eax
            sub eax, 8192         ; eax -> ebp-8192
            push offset backslash ; backslash ("\")
            push eax              ; next_path
            call crt_strcat       ; wcscat(next_path, L"\\");

            mov eax, ebp          ; sauvegarde le pointeur de bas de pile dans eax
            sub eax, 8192         ; eax -> ebp-8192
            mov ebx, ebp          ; sauvegarde le pointeur de bas de pile dans ebx
            sub ebx, 8784         ; ebx -> ebp-8784
            add ebx, 44           ; offset for cFileName
            push ebx              ; push eax on the stack (find_file_data.cFileName)
            push eax              ; next_path
            call crt_strcat       ; wcscat(next_path, find_file_data.cFileName);

            mov eax, [ebp+12]
            inc eax
            mov ebx, ebp          ; sauvegarde le pointeur de bas de pile dans ebx
            sub ebx, 8192         ; ebx -> ebp-8192
            push eax              ; index + 1
            push ebx              ; next_path
            call recdir           ; (next_path, index + 1);

            jmp next

        file_prompt:
            mov eax, ebp           ; sauvegarde le pointeur de bas de pile dans eax
            sub eax, 8784          ; eax -> ebp-8784
            add eax, 44            ; offset for cFileName
            push eax               ; push eax on the stack (find_file_data.cFileName)
            push offset filePrompt ; push eax on the stack (find_file_data.cFileName)
            call crt_printf        ; printf("%s\n", find_file_data.cFileName);

        next:
            mov eax, [ebp-8788]  ; listing_handler
            mov ebx, ebp         ; sauvegarde le pointeur de bas de pile dans eax
            sub ebx, 8784        ; eax -> ebp-8784 (&find_file_data)
            push ebx
            push eax
            call FindNextFile    ; FindNextFileW(listing_handler, &find_file_data)
            test eax, eax
            jnz list_file

        return:
            mov esp, ebp  ; retour sur l'ancienne stackframe
            pop ebp       ; place la valeur contenue dans ebp sur la stack (adresse de retour)
            ret           ; retour à l'adresse indiquée sur la stack (précédemment dans ebp)
    recdir ENDP

    start:
        push offset userInputPrompt
        call crt_printf ; Affiche le prompt pour la saisie utilisateur
                        ; équivalent en C : wprintf(L"Your directory ? #> ");

        push offset initPath
        push offset scanfFormat
        call crt_scanf ; récupère la saisie utilisateur depuis l'entrée standard
                       ; et la stocke dans userText
                       ; équivalent en C : scanf("%s", &initPath);

        push offset initPath
        push offset targetDirectoryPrompt
        call crt_printf  ; Affiche le prompt pour la saisie utilisateur
                         ; équivalent en C : printf("\nTarget directory is %s\n", init_path);

        push 0
        push offset initPath
        call recdir ; appel de la fonction recdir avec la saisie de l'utilisateur en paramètre

        mov eax, 0
        invoke ExitProcess, eax ; met fin à l'exécution du programme
                                ; et retourne un code d'erreur à 0
                                ; équivalent en C : exit(0);

    end start
